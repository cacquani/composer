# frozen_string_literal: true

class User
  attr_reader :username
  def initialize(username)
    @username = username
  end

  def user
    username
  end
end
