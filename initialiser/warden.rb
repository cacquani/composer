# frozen_string_literal: true

require 'warden'
require './models/user'

strategy = Class.new(Warden::Strategies::Base)

strategy.send(:define_method, :authenticate!) do
  if params['username'] == 'test' &&
     params['password'] == 'test'
    user = User.new(params['username'])
    success!(user)
    env['warden'].set_user(@user)
    redirect! '/'
  else
    fail! 'Could not log in. Username or password incorrect.'
    redirect! '/session/new'
  end
end

Warden::Strategies.add(:password, strategy)

Warden::Manager.before_failure do |env, _opts|
  env['REQUEST_METHOD'] = 'POST'
  env.each do |key, _value|
    env[key]['_method'] = 'post' if key == 'rack.request.form_hash'
  end
end

# method override for put, delete
Cuba.use Rack::MethodOverride
Cuba.use Warden::Manager do |manager|
  manager.serialize_into_session(&:user)
  manager.serialize_from_session { |user| user }
  manager.failure_app = Sessions
  manager.scope_defaults :default,
                         strategies: [:password],
                         action: 'session/unauthenticated'
end
