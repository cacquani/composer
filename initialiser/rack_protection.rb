# frozen_string_literal: true

require 'rack/protection'
require 'cuba/safe'

Cuba.use Rack::Session::Cookie,
         secret: 'CTUfGzZjNt1l+##PYWHM7AW_JUdm-21w0wFxMKH33B!FhK'
Cuba.use Rack::Protection
Cuba.use Rack::Protection::RemoteReferrer

Cuba.plugin Cuba::Safe
