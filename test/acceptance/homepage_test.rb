# frozen_string_literal: true

require_relative '../acceptance_helper'

scope 'Homepage' do
  setup do
    get '/'
  end

  test 'should redirect to login' do
    assert_equal 'You are being redirected to /session/new', last_response.body
  end
end
