# frozen_string_literal: true

require_relative '../acceptance_helper'

scope 'should interprete scss to css' do
  setup do
    get '/stylesheets/home.css'
  end

  test 'should be successful' do
    assert_equal 200, last_response.status
  end

  test 'should be a text/css' do
    assert(/^text\/css/.match(last_response.headers['Content-Type']))
  end

  test 'should have body' do
    assert(/^body/.match(last_response.body))
  end
end

scope 'should serve plain css' do
  setup do
    get '/stylesheets/test.css'
  end

  test 'should be successful' do
    assert_equal 200, last_response.status
  end

  test 'should be a text/css' do
    assert(/^text\/css/.match(last_response.headers['Content-Type']))
  end

  test 'should have body' do
    assert(/^body/.match(last_response.body))
  end
end
