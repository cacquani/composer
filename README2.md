# Picture sharing with friends and fans? #

## Essentials ##

1.  Single Sign On
    1. Proprietary sign on
    2. Facebook sign on
    3. Google/OAuth sign on

2.  Entities
    1. Single User
    2. Public Entity
    3. Private Entity

3.  Relations
    1.  User
        1. Has Many Friends
        2. Has Many Followed
        3. Has Many Followers
    2.  Public Entity
        1. Has Many Followed (only entities)
        2. Has Many Followers (entities or users)
        3. When two entities follow each other, they become "twin entities"

4.  Actions
    1. Can publish pictures
    2. Can publish posts?
    3. Can comment
    4. Can like/dislike/simpathise? Ask Dario later.
    5. Can search
    6. Can organise their followers and friends

## Code ##

### My Account interface ###

Log in connecting with sign-on app.

Get all the account information for view.

Edit the account information.

### Sign-on application ###

Webservice. 

Can check internally or connect to FB/OAuth.

merbist.com/2012/04/04/building-and-implementing-a-single-sign-on-solution

Actions: 

1.  Register
2.  Login
3.  Logout
4.  Remember-me
5.  Unregister
6.  Token


### Resources ###

