# frozen_string_literal: true

require 'slim'

Main = Cuba.new do
  settings[:render][:template_engine]  = 'slim'
  settings[:render][:views]            = 'app/views'

  on get do
    env['warden'].authenticate!
    render 'home/index', user: 'username'
  end
end
