# frozen_string_literal: true

# Serves plain css stylesheets
# or interprete sacs to css
require 'sass'

Stylesheets = Cuba.new do
  settings[:render][:template_engine]  = 'scss'
  settings[:render][:views]            = 'app/apps/stylesheets/views'

  on get do
    on '([^\/]+).css' do |style|
      file = File.join(settings[:render][:views], "#{style}.css")
      if File.exist?(file)
        res.headers['Content-Type'] = 'text/css; charset=utf-8'
        res.write File.read(file)
      else
        res.headers['Content-Type'] = 'text/css; charset=utf-8'
        res.write partial(style.to_s)
      end
    end
  end
end
