var Home = {
  initTogglers: function() {
    $('.toggle').find('h3').addClass('minimised');
    $('.toggle').find('h3').click(Home.toggleSection);
    $('.toggle').children('.panel-body').hide();
  },

  toggleSection: function(event) {
    if($(event.target).hasClass('minimised')) {
      $(event.target).removeClass('minimised');
      $(event.target).addClass('open');
      $(event.target).parents('.toggle').children('.panel-body').show();
    } else {
      $(event.target).removeClass('open');
      $(event.target).addClass('minimised');
      $(event.target).parents('.toggle').children('.panel-body').hide();
    }
  }
}

$(document).ready(Home.initTogglers);
