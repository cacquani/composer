# frozen_string_literal: true

# For now, it serves plain js.
# Maybe extend for coffee script later?
Javascripts = Cuba.new do
  settings[:views] = 'app/apps/javascripts/views'

  on get do
    on '([^\/]+).js' do |script|
      file = File.join(settings[:views], "#{script}.js")
      if File.exist?(file)
        res.headers['Content-Type'] = 'text/javascript; charset=utf-8'
        res.write File.read(file)
      end
    end
  end
end
