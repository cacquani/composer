# frozen_string_literal: true

require 'slim'

Sessions = Cuba.new do
  settings[:render][:template_engine]  = 'slim'
  settings[:render][:views]            = 'app/views'

  on get do
    on 'new' do
      res.redirect '/' unless env['warden'].unauthenticated?
      render 'sessions/new'
    end

    on root do
      res.redirect '/session/new'
    end
  end

  on post do
    on 'unauthenticated' do
      session['return_to'] = env['warden.options'][:attempted_path]
      res.redirect '/session/new'
    end

    on root do
      env['warden'].authenticate!
      res.redirect '/'
    end
  end

  on delete do
    on '/' do
      env['warden'].raw_session.inspect
      env['warden'].logout
      res.redirect '/'
    end
  end
end
