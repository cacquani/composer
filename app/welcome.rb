# frozen_string_literal: true

require 'cuba'

# require all initialisers
require './initialiser/index'

# require all the apps
Dir['./app/apps/**/*.rb'].each do |app|
  require app
end

Cuba.define do
  on csrf.unsafe? do
    csrf.reset!
    res.status = 403
    res.write 'Not authorised!'
    halt res.finish
  end

  # session.delete :csrf_token

  on 'stylesheets' do
    run Stylesheets
  end

  on 'javascripts' do
    run Javascripts
  end

  on 'session' do
    run Sessions
  end

  on root do
    run Main
  end
end
